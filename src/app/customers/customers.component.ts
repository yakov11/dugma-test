import { Component,OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomersService } from '../customers.service';
import { Customer } from '../interfaces/customer';
import { PredictService } from '../predict.service';

@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  customers:Customer[];
  customers$;
  userId:string;
  addCustomerFormOpen= false;

  years:number;
  income:number;
  
id:string;

  prediction='unknown';


  predict(years,income){
    console.log(years,income);
    this.predictService.classify(years,income).subscribe(
      res => {
        console.log(res);
      if(res>0.5){
        this.prediction='Yes'
      }else{
        this.prediction='No'
      }return this.prediction;
     }
      )

      }
  showPrediction(){
    return this.prediction;
  }

  editstate = [];
  constructor(private customersService:CustomersService, public authService:AuthService,private predictService:PredictService ) { }


  deleteCustomer(id:string){
    this.customersService.deleteCustomer(this.userId, id);
    console.log(id);
  } 
  updateCustomer(customer:Customer){
    this.customersService.updateCustomer(this.userId, customer.id, customer.name, customer.years,customer.income)
  }
  
  add(customer:Customer){
    this.customersService.addCustomer(this.userId, customer.name, customer.years,customer.income);
  }

  addPrediction(userId,id,prediction){
    console.log(id);
    console.log(userId);
    console.log(prediction);


    this.customersService.addPredict(userId,id,prediction)
  }
  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.customers$ = this.customersService.getCustomers(this.userId);
        
    })}

}








  

  
