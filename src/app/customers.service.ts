import { AngularFirestoreModule, AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  customersCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection=this.db.collection('users');

  getCustomers(userId):Observable<any[]>{
    this.customersCollection = this.db.collection(`users/${userId}/customers`);
    return this.customersCollection.snapshotChanges().pipe(
      map(
        collection =>collection.map(
          document=> {
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            return data; 
          }
        )  
     ))  
  }


  deleteCustomer(userId:string, id:string){
    this.db.doc(`users/${userId}/customers/${id}`).delete();
     
  }
  addCustomer(userId:string, name:string, years:number, income:number ){
    const customer = {name:name, years:years,income:income};
    this.userCollection.doc(userId).collection('customers').add(customer);
  }

  public updateCustomer(userId:string, id:string, name:string, years:number, income:number){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        years:years,
        income:income,
        

      }
    )

  }

  public addPredict(userId:string,id:string,prediction:string){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        prediction:prediction
      }
    )
  }
  

  constructor(private db:AngularFirestore) { }
}







