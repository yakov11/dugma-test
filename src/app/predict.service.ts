import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PredictService {

  private url ="https://pm3syupnh3.execute-api.us-east-1.amazonaws.com/beta";

  classify(years:number,income:number){
    let json=
      {'years':years,
       'income':income
    }
    let body = JSON.stringify(json);

    
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        

        return res; 
      })
    )
  }

  constructor(private http:HttpClient) { }
}
